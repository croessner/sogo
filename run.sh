#!/usr/bin/env sh

/usr/sbin/nginx -c /etc/nginx/nginx-docker.conf

exec su sogo -s /bin/sh -c "/usr/local/sbin/sogod -WONoDetach YES -WOPort 20000 -WOLogFile - -WOPidFile /tmp/sogo.pid"
