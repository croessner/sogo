FROM debian:11-slim

ENV DEBIAN_FRONTEND noninteractive

ARG version=5.6.0
ARG wbxml_version=0.11.8

LABEL org.opencontainers.image.authors="christian@roessner.email"
LABEL com.roessner-network-solutions.vendor="Rößner-Network-Solutions"
LABEL version="${version}"
LABEL description="SOGo is a fully supported and trusted groupware server with a focus on scalability and open standards"

WORKDIR /build

# Download SOPE sources
ADD https://github.com/inverse-inc/sope/archive/SOPE-${version}.tar.gz /build/src/SOPE/SOPE.tar.gz

# Download SOGo sources
ADD https://github.com/inverse-inc/sogo/archive/SOGo-${version}.tar.gz /build/src/SOGo/SOGo.tar.gz

# For ActiveSync - libwbxml
ADD https://github.com/libwbxml/libwbxml/archive/libwbxml-${wbxml_version}.tar.gz /build/src/wbxml2.tar.gz

RUN set -ex; \
  echo "Untar SOPE sources"; \
  tar -xf /build/src/SOPE/SOPE.tar.gz; \
  echo "Untar SOGO sources"; \
  tar -xf /build/src/SOGo/SOGo.tar.gz; \
  echo "Untar wbxml sources"; \
  tar -xf /build/src/wbxml2.tar.gz; \
  echo "Install required packages"; \
  apt-get update; \
  apt-get install -qy --no-install-recommends \
      gnustep-make \
      gnustep-base-common \
      libgnustep-base-dev \
      make \
      cmake \
      gobjc \
      libxml2-dev \
      libexpat1-dev \
      libssl-dev \
      libldap2-dev \
      zlib1g-dev \
      postgresql-server-dev-13 \
      libmemcached-dev \
      libsodium-dev \
      libzip-dev \
      liboath-dev \
      libcurl4-openssl-dev \
      libytnef0-dev \
      nginx \
      tzdata \
      ca-certificates \
      netcat-traditional \
      curl; \
  echo "Source /usr/share/GNUstep/Makefiles/GNUstep.sh"; \
  . /usr/share/GNUstep/Makefiles/GNUstep.sh; \
  echo "Compiling wbxml"; \
  cd /build/libwbxml-libwbxml-${wbxml_version}; \
  cmake . -B/build/libwbxml; \
  cd /build/libwbxml; \
  make; \
  make install; \
  echo "Compiling SOPE"; \
  cd /build/sope-SOPE-${version};  \
  ./configure --with-gnustep --enable-debug; \
  make; \
  make install; \
  echo "Compiling SOGo"; \
  cd /build/sogo-SOGo-${version}; \
  ./configure --enable-debug --enable-mfa; \
  make; \
  make install; \
  echo "Compiling ActiveSync"; \
  cd /build/sogo-SOGo-${version}/ActiveSync; \
  ADDITIONAL_INCLUDE_DIRS="-I/usr/local/include/libwbxml-1.0/" make; \
  make install; \
  echo "Adding lib directories"; \
  echo "/usr/local/lib" > /etc/ld.so.conf.d/local.conf; \
  echo "/usr/local/lib/sogo" > /etc/ld.so.conf.d/sogo.conf; \
  ldconfig; \
  echo "Create user sogo"; \
  groupadd --system --gid 999 sogo && useradd --system --uid 999 --gid sogo sogo; \
  echo "Create directories and enforce permissions"; \
  install -o sogo -g sogo -m 755 -d /run/sogo; \
  install -o sogo -g sogo -m 750 -d /var/spool/sogo; \
  echo "Removing unused files and directories"; \
  apt-mark manual \
      gnustep-make \
      libcurl4 \
      libgcc1 \
      libglib2.0-0 \
      libgnustep-base1.27 \
      libldap-2.4-2 \
      libmemcached11 \
      libsodium23 \
      libzip4 \
      liboath0 \
      libobjc4 \
      libpq5 \
      libssl1.1 \
      libxml2 \
      libexpat1 \
      zlib1g \
      postgresql-client-common \
      postgresql-common > /dev/null; \
  apt-get purge -y --auto-remove -o APT::AutoRemove::RecommendsImportant=false \
      gobjc \
      libcurl4-openssl-dev \
      libgnustep-base-dev \
      libldap2-dev \
      libmemcached-dev \
      libssl-dev \
      libxml2-dev \
      libexpat1-dev \
      zlib1g-dev \
      make \
      cmake \
      postgresql-server-dev-13; \
  rm -rf /var/lib/apt/lists/*; \
  rm -rf /build

COPY ./nginx/nginx-docker.conf /etc/nginx/nginx-docker.conf

COPY ./run.sh /

EXPOSE 80 20000

CMD [ "/run.sh" ]

